import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifme/add_notification.dart';
import 'package:notifme/group_member.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'const.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatGroup extends StatelessWidget {
  final String peerId;
  final String peerAvatar;
  final String groupName;
  final String nickName;
  final String adminId;
  final String currentUserId;

  const ChatGroup({Key key, this.peerId, this.peerAvatar, this.groupName, this.nickName, this.adminId, this.currentUserId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          groupName,
          style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.group),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => GroupMember(groupId: peerId,)));
              }),
          (adminId == currentUserId) ?
          new IconButton(
              icon: new Icon(Icons.add_alert),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => AddNotification(idGroup: peerId,)));
              }
          ) : new IconButton(
                icon: new Icon(Icons.exit_to_app),
                onPressed: () {
        //                Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(currentUserId: currentUserId)));
                }
          ),

        ],
        centerTitle: true,
      ),
      body: new ChatGroupPage(
        peerId: peerId,
        peerAvatar: peerAvatar,
        groupName: groupName,
        nickName: nickName,
        adminId: adminId,
      ),
    );
  }
}

class ChatGroupPage extends StatefulWidget {
  final String peerId;
  final String peerAvatar;
  final String groupName;
  final String nickName;
  final String adminId;

  const ChatGroupPage({Key key, @required this.peerId, @required this.peerAvatar, @required this.groupName, @required this.nickName, @required this.adminId}) : super(key: key);

  @override
  _ChatGroupPageState createState() => _ChatGroupPageState(peerId: peerId, peerAvatar: peerAvatar, groupName: groupName, nickName: nickName, adminId: adminId);
}

class _ChatGroupPageState extends State<ChatGroupPage> {

  _ChatGroupPageState({Key key, @required this.peerId, @required this.peerAvatar, @required this.groupName, @required this.nickName, @required this.adminId});

  String peerId;
  String peerAvatar;
  String id;
  String groupName;
  String nickName;
  String adminId;

  var listMessage;
  String groupChatId;
  SharedPreferences prefs;

  File imageFile;
  bool isLoading;
  bool isShowSticker;
  String imageUrl;

  final TextEditingController textEditingController = new TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  final FocusNode focusNode = new FocusNode();

  @override
  void initState(){
    super.initState();
    focusNode.addListener(onFocusChange);

    groupChatId = '';
    isLoading = false;
    isShowSticker = false;
    imageUrl = '';

    readLocal();
  }

  void onFocusChange(){
    if (focusNode.hasFocus){
      setState(() {
        isShowSticker = false;
      });
    }
  }

  readLocal() async{
    prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id') ?? '';
    if (id.hashCode <= peerId.hashCode){
      groupChatId = '$peerId+$groupName';
    } else {
      groupChatId = '$peerId+$groupName';
    }
    setState(() {

    });
  }

  Future getImage() async{
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (imageFile != null){
      setState(() {
        isLoading = true;
      });
      uploadFile();
    }
  }

  void getSticker(){
    focusNode.unfocus();
    setState(() {
      isShowSticker = !isShowSticker;
    });
  }

  Future uploadFile() async{
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = reference.putFile(imageFile);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl){
      imageUrl = downloadUrl;
      setState(() {
        isLoading = false;
        onSendMessage(imageUrl, 1);
      });
    }, onError: (err){
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: 'This file is not an image');
    });
  }

  void onSendMessage(String content, int type){
    if (content.trim() != ''){
      textEditingController.clear();

      Firestore.instance
          .collection('messages')
          .document(groupChatId)
          .collection(groupChatId)
          .document(DateTime.now().millisecondsSinceEpoch.toString())
          .setData({
        'idFrom': id,
        'idTo': peerId,
        'idAvatar' : peerAvatar,
        'nameFrom' : nickName,
        'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
        'content':content,
        'type':type
      });
      listScrollController.animateTo(0.0, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    } else{
      Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }

  Widget buildItem(int index, DocumentSnapshot document){
    if (document['idFrom'] == id){
      return Row(
        children: <Widget>[
          document['type'] == 0
              ? Container(
            child: Text(
              document['content'],
              style: TextStyle(color: primaryColor),
            ),
            padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
            width: 200.0,
            decoration: BoxDecoration(color: greyColor2, borderRadius: BorderRadius.circular(8.0)),
            margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
          )
              : document['type'] == 1
              ? Container(
            child: Material(
              child: CachedNetworkImage(
                // placeholder: Container(
                //   child: CircularProgressIndicator(
                //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                //   ),
                //   width: 200.0,
                //   height: 200.0,
                //   padding: EdgeInsets.all(70.0),
                //   decoration: BoxDecoration(
                //     color: greyColor2,
                //     borderRadius: BorderRadius.all(Radius.circular(8.0))
                //   ),
                // ),
                // errorWidget: Material(
                //   child: Image.asset(
                //     'images/img_not_available.jpeg',
                //     width: 200.0,
                //     height: 200.0,
                //     fit: BoxFit.cover,
                //   ),
                //   borderRadius: BorderRadius.all(
                //     Radius.circular(8.0),
                //   ),
                //   clipBehavior: Clip.hardEdge,
                // ),
                imageUrl: document['content'],
                width: 200.0,
                height: 200.0,
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              clipBehavior: Clip.hardEdge,
            ),
            margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
          )
              : Container(
            child: new Image.asset(
              'images/${document['content']}.gif',
              width: 100.0,
              height: 100.0,
              fit: BoxFit.cover,
            ),
            margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.end,
      );
    } else {
      return Container(
        child: Column(
          children: <Widget>[
            Container(
              child: Text(
                document['nameFrom'],
                style: TextStyle(color: themeColor),
              ),
              margin: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
            ),
            Column(
              children: <Widget>[
                document['type'] == 0
                    ? Container(
                  child: Text(
                    document['content'],
                    style: TextStyle(color: Colors.white),
                  ),
                  padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  width: 200.0,
                  decoration: BoxDecoration(color: primaryColor, borderRadius: BorderRadius.circular(8.0)),
                  margin: EdgeInsets.only(left: 10.0),
                )
                    : document['type'] == 1
                    ? Container(
                  child: Material(
                    child: CachedNetworkImage(
                      // placeholder: Container(
                      //   child: CircularProgressIndicator(
                      //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      //   ),
                      //   width: 200.0,
                      //   height: 200.0,
                      //   padding: EdgeInsets.all(70.0),
                      //   decoration: BoxDecoration(
                      //     color: greyColor2,
                      //     borderRadius: BorderRadius.all(
                      //       Radius.circular(8.0)
                      //     )
                      //   ),
                      // ),
                      // errorWidget: Material(
                      //   child: Image.asset(
                      //     'images/img_not_available.jpeg',
                      //     width: 200.0,
                      //     height: 200.0,
                      //     fit: BoxFit.cover,
                      //   ),
                      //   borderRadius: BorderRadius.all(
                      //     Radius.circular(8.0)
                      //   ),
                      //   clipBehavior: Clip.hardEdge,
                      // ),
                      imageUrl: document['content'],
                      width: 200.0,
                      height: 200.0,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    clipBehavior: Clip.hardEdge,
                  ),
                  margin: EdgeInsets.only(left: 10.0),
                )
                    : Container(
                  child: new Image.asset(
                    'images/${document['content']}.gif',
                    width: 100.0,
                    height: 100.0,
                    fit: BoxFit.cover,
                  ),
                  margin: EdgeInsets.only(bottom: isLastMessageRight(index) ? 20.0 : 10.0, right: 10.0),
                )
              ],

            ),
            Container(
              child: Text(
                DateFormat('dd MMM kk:mm')
                    .format(DateTime.fromMillisecondsSinceEpoch(int.parse(document['timestamp']))),
                style: TextStyle(color: greyColor, fontSize: 12.0, fontStyle: FontStyle.italic),
              ),
              margin: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
            )
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
        margin: EdgeInsets.only(bottom: 10.0),
      );
    }
  }

  bool isLastMessageLeft(int index){
    if ((index > 0 && listMessage != null && listMessage[index - 1]['idFrom'] == id) || index == 0){
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index){
    if ((index > 0 && listMessage != null && listMessage[index-1]['idFrom'] != id) || index == 0){
      return true;
    } else {
      return false;
    }
  }

  Future<bool> onBackPress(){
    if (isShowSticker){
      setState(() {
        isShowSticker = false;
      });
    } else {
      Navigator.pop(context);
    }
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              buildListMessage(),
              (isShowSticker ? buildSticker() : Container()),

              buildInput(),
            ],
          ),

          buildLoading()
        ],
      ),
      onWillPop: onBackPress,
    );
  }

  Widget buildSticker(){
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi1', 2),
                child: new Image.asset(
                  'images/mimi1.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi2', 2),
                child: new Image.asset(
                  'images/mimi2.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi3', 3),
                child: new Image.asset(
                  'images/mimi3.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi4', 2),
                child: new Image.asset(
                  'images/mimi4.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi5', 2),
                child: new Image.asset(
                  'images/mimi5.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi6', 2),
                child: new Image.asset(
                  'images/mimi6.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi7', 2),
                child: new Image.asset(
                  'images/mimi7.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi8', 2),
                child: new Image.asset(
                  'images/mimi8.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi9', 2),
                child: new Image.asset(
                  'images/mimi9.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      decoration: new BoxDecoration(
          border: new Border(top: new BorderSide(color: greyColor2, width: 0.5)), color: Colors.white
      ),
      padding: EdgeInsets.all(5.0),
      height: 180.0,
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: isLoading
          ? Container(
        child: Center(
          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
        ),
        color: Colors.white.withOpacity(0.8),
      )
          : Container(),
    );
  }

  Widget buildInput(){
    return Container(
      child: Row(
        children: <Widget>[
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0),
              child: new IconButton(
                icon: new Icon(Icons.image),
                onPressed: getImage,
                color: primaryColor,
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0),
              child: new IconButton(
                icon: new Icon(Icons.face),
                onPressed: getSticker,
                color: primaryColor,
              ),
            ),
            color: Colors.white,
          ),
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(color: primaryColor, fontSize: 15.0),
                controller: textEditingController,
                decoration: InputDecoration.collapsed(
                    hintText: 'Type your message...',
                    hintStyle: TextStyle(color: greyColor)
                ),
                focusNode: focusNode,
              ),
            ),
          ),
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 8.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => onSendMessage(textEditingController.text, 0),
                color: primaryColor,
              ),
            ),
            color: Colors.white,
          )
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: new BoxDecoration(
          border: new Border(top: new BorderSide(color: greyColor2, width: 0.5)), color: Colors.white
      ),
    );
  }

  Widget buildListMessage() {
    return Flexible(
      child: groupChatId == ''
          ? Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)))
          : StreamBuilder(
        stream: Firestore.instance
            .collection('messages')
            .document(groupChatId)
            .collection(groupChatId)
            .orderBy('timestamp', descending: true)
            .limit(20)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)));
          } else {
            listMessage = snapshot.data.documents;
            return ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemBuilder: (context, index) => buildItem(index, snapshot.data.documents[index]),
              itemCount: snapshot.data.documents.length,
              reverse: true,
              controller: listScrollController,
            );
          }
        },
      ),
    );
  }
}
