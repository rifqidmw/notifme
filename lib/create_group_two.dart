import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifme/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'const.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'model/friend_model.dart';

class CreateGroupTwo extends StatelessWidget {
  final String currentUserId;
  final String currentGroupId;

  const CreateGroupTwo({Key key, this.currentUserId, this.currentGroupId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Create a Group"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(currentUserId: currentUserId)));
              }
          ),
          new IconButton(
            icon: new Icon(Icons.check),
            onPressed: () {
              Fluttertoast.showToast(msg: "Success create a group");
              Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(currentUserId: currentUserId)));
            }

//            Navigator.pop(context),
          ),
        ],
      ),
      body: new CreateGroupTwoScreen(
        currentUserId: currentUserId,
        currentGroupId: currentGroupId,
      ),
    );
  }
}

class CreateGroupTwoScreen extends StatefulWidget {
  final String currentUserId;
  final String currentGroupId;

  const CreateGroupTwoScreen({Key key, this.currentUserId, this.currentGroupId}) : super(key: key);

  @override
  State createState() => new CreateGroupTwoState(currentUserId: currentUserId, currentGroupId: currentGroupId);
}

class CreateGroupTwoState extends State<CreateGroupTwoScreen> {
  final String currentUserId;
  final String currentGroupId;

  bool isLoading = false;
  List<FriendModel> dataList = new List();
  List<FriendModel> backDataList = new List();

  CreateGroupTwoState({Key key, @required this.currentUserId, @required this.currentGroupId});

  @override
  void initState() {
    super.initState();
  }

  TextEditingController editingController = TextEditingController();

  void filterSearchResults(String query) {
    List<FriendModel> dummySearchList = List<FriendModel>();
    dummySearchList.addAll(dataList);
    if (query.isNotEmpty) {
      List<FriendModel> dummyListData = List<FriendModel>();
      dummySearchList.forEach((item) {
        if (dataList.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        dataList.clear();
        dataList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        dataList.clear();
        dataList.addAll(backDataList);
      });
    }
  }

  void loadData(DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
  }

  Widget buildSelectedItem(BuildContext context, DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    friendModel.setToken = document['token'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
    return Container(
      child: FlatButton(
        child: Column(
          children: <Widget>[
            Material(
              child: CachedNetworkImage(
                // placeholder: Container(
                //   child: CircularProgressIndicator(
                //     strokeWidth: 1.0,
                //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                //   ),
                //   width: 50.0,
                //   height: 50.0,
                //   padding: EdgeInsets.all(15.0),
                // ),

                imageUrl: friendModel.get_photoUrl,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
              clipBehavior: Clip.hardEdge,
            ),
            new Expanded(
              child: Container(
                child: new Center(
                  child: new Container(
                    child: Text(
                      '${friendModel.get_nickname}',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: primaryColor),
                    ),
                    alignment: Alignment.center,
//                    margin: new EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                  ),
                ),
                margin: EdgeInsets.only(top: 5.0),
              ),
            ),
          ],
        ),
        onPressed: () {
          _deleteSelectedItem(friendModel.get_nickname, friendModel.get_email,
              friendModel.get_photoUrl, friendModel.get_id, friendModel.get_token);
        },
//        color: greyColor2,
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
//        shape:
//              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      margin: EdgeInsets.only(bottom: 5.0, left: 5.0, right: 5.0, top: 5.0),
    );
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    friendModel.setToken = document['token'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
    if (friendModel.get_id == currentUserId) {
      return Container();
    } else {
      return Container(
        child: FlatButton(
          child: Row(
            children: <Widget>[
              Material(
                child: CachedNetworkImage(
                  // placeholder: Container(
                  //   child: CircularProgressIndicator(
                  //     strokeWidth: 1.0,
                  //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  //   ),
                  //   width: 50.0,
                  //   height: 50.0,
                  //   padding: EdgeInsets.all(15.0),
                  // ),

                  imageUrl: friendModel.get_photoUrl,
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                clipBehavior: Clip.hardEdge,
              ),
              new Flexible(
                child: Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        child: Text(
                          'Nickname: ${friendModel.get_nickname}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                      new Container(
                        child: Text(
                          '${friendModel.get_email ?? 'Not available'}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
          onPressed: () {
            _selectedItem(friendModel.get_nickname, friendModel.get_email,
                friendModel.get_photoUrl, friendModel.get_id, friendModel.get_token);
          },
          color: greyColor2,
          padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
    }
  }

  void _selectedItem(String nickname, String email, String photoUrl, String id, String token) {
    final collRef = Firestore.instance.collection('groups');
    DocumentReference docReferance = collRef.document(currentGroupId);
    docReferance.updateData({'members': FieldValue.arrayUnion([id])});
    Firestore.instance
        .collection('groups')
        .document(currentGroupId)
        .collection('members')
        .document(id)
        .setData({
      'email': email,
      'nickname': nickname,
      'photoUrl': photoUrl,
      'id': id
    });

    addGroup(currentGroupId, token, email);
  }

  void _deleteSelectedItem(String nickname, String email, String photoUrl, String id, String token) {
    Firestore.instance
        .collection('groups')
        .document(currentGroupId)
        .collection('members')
        .document(id)
        .delete();

    deleteFromGroup(currentGroupId, token, email);
  }

  void addGroup(String idGroup, String token, String email) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://172.27.13.19/firebasenoification";

    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "token": token,
      "email": email
    });

    response = await dio.post("/RegisterGroup.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }
  void deleteFromGroup(String idGroup, String token, String email) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://172.27.13.19/firebasenoification";
    debugPrint("delete from id: $idGroup and email: $email");
    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "email": email
    });

    response = await dio.post("/deleteFromGroup.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      child: Column(
        children: <Widget>[
          StreamBuilder(
              stream: Firestore.instance.collection('groups').document(currentGroupId).collection('members').snapshots(),
              builder: (context, snapshot){
                debugPrint('user id: $currentUserId');
                debugPrint('group id in two: $currentGroupId');
                if (!snapshot.hasData){
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                    ),
                  );
                } else {
                  return GridView.builder(
                      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                      shrinkWrap: true,
                      itemBuilder: (context, index) => buildSelectedItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                  );
                }
              },
            ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              onChanged: (value) {
                filterSearchResults(value);
              },
              controller: editingController,
              decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
          Expanded(
            child: StreamBuilder(
              stream: Firestore.instance.collection('users').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                    ),
                  );
                } else {
                  return ListView.builder(
                    padding: EdgeInsets.all(10.0),
                    itemBuilder: (context, index) =>
                        buildItem(context, snapshot.data.documents[index]),
                    itemCount: snapshot.data.documents.length,
                    shrinkWrap: true,
                  );
                }
              },
            ),
          ),
          Positioned(
            child: isLoading
                ? Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    ),
                    color: Colors.white.withOpacity(0.8),
                  )
                : Container(),
          ),
        ],
      ),
      // onWillPop: onBackPress,
    );
  }
}
