class FriendModel{
  String _nickname;
  String _email;
  String _photoUrl;
  String _id;
  String _token;

  String get get_nickname => _nickname;
  String get get_email => _email;
  String get get_photoUrl => _photoUrl;
  String get get_id => _id;
  String get get_token => _token;

  void set setNickname(String nickname){
    _nickname = nickname;
  }
  void set setEmail(String email){
    _email = email;
  }
  void set setPhotoUrl(String photoUrl){
    _photoUrl = photoUrl;
  }
  void set setId(String id){
    _id = id;
  }
  void set setToken(String token){
    _token = token;
  }
}