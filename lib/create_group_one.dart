
import 'package:notifme/create_group_two.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'const.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateGroupOne extends StatelessWidget {
  final String currentUserId;

  const CreateGroupOne({Key key, this.currentUserId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Create a Group"),
      ),
      body: new CreateGroupOneScreen(currentUserId: currentUserId,),
    );
  }
}

class CreateGroupOneScreen extends StatefulWidget {
  final String currentUserId;

  const CreateGroupOneScreen({Key key, this.currentUserId}) : super(key: key);
  @override
  State createState() => new CreateGroupOneState(currentUserId: currentUserId);
}

class CreateGroupOneState extends State<CreateGroupOneScreen> {

  final String currentUserId;

  CreateGroupOneState({Key key, @required this.currentUserId});

  final GoogleSignIn googleSignIn = new GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  TextEditingController controllerNameGroup;

  bool isLoading = false;
  File avatarImageFile;
  SharedPreferences prefs;

  String idGroup = '';
  String nameGroup = '';
  String photoUrlGroup = '';
  String id = '';
  String nickname = '';
  String email = '';
  String aboutMe = '';
  String photoUrl = '';
  String token = '';

  final FocusNode focusNodeNameGroup = new FocusNode();

  @override
  void initState() {
    super.initState();
    readLocal();
  }

  void readLocal() async {
    prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id') ?? '';
    nickname = prefs.getString('nickname') ?? '';
    email = prefs.getString('email') ?? '';
    aboutMe = prefs.getString('aboutMe') ?? '';
    photoUrl = prefs.getString('photoUrl') ?? '';
    token = prefs.getString('token') ?? '';

    controllerNameGroup = new TextEditingController(text: nameGroup);

  }

  Future getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        avatarImageFile = image;
        idGroup = image.path;
        isLoading = true;
      });
    }
    uploadFile();
  }

  Future uploadFile() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = reference.putFile(avatarImageFile);
    StorageTaskSnapshot storageTaskSnapshot;
    uploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;
        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          setState(() {
            photoUrlGroup = downloadUrl;
            Fluttertoast.showToast(msg: "Upload success");
            isLoading = false;
          });
        }, onError: (err) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(msg: 'This file is not an image');
        });
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: 'This file is not an image');
      }
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  Future saveData() async {
    final collRef = Firestore.instance.collection('groups');
    DocumentReference docReferance = collRef.document();
    docReferance.setData({'nameGroup': nameGroup, 'photoUrl': photoUrlGroup, 'members': FieldValue.arrayUnion([id]), 'adminId': currentUserId, 'adminName': nickname, 'adminEmail': email, 'idGroup': docReferance.documentID}).then((data) async {
      setState(() {
        isLoading = true;
        Firestore.instance.collection('groups').document(docReferance.documentID).collection('members').document(currentUserId).setData({
          'email' : email,
          'nickname' : nickname,
          'photoUrl' : photoUrl,
          'id' : id
        }).then((data) async{
          setState(() {
            Navigator.push(context, MaterialPageRoute(builder: (context) => CreateGroupTwo(currentUserId: currentUserId, currentGroupId: docReferance.documentID,)));
            isLoading = false;
          });
        }).catchError((err){
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(msg: err.toString());
        });
      });

      Future<dynamic> firebaseMessaging = FirebaseMessaging().getToken().then((token){
        debugPrint('token: '+token);
        addGroup(docReferance.documentID, token, email);
      }).catchError((e){
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: e.toString());
      });

    }).catchError((err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  void addGroup(String idGroup, String token, String email) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://172.27.13.19/firebasenoification";

    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "token": token,
      "email": email
    });

    response = await dio.post("/RegisterGroup.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Avatar
              Container(
                child: Center(
                  child: Stack(
                    children: <Widget>[
                      (avatarImageFile == null)
                          ? (photoUrlGroup != ''
                          ? Material(
                        child: CachedNetworkImage(
                          imageUrl: photoUrlGroup,
                          width: 90.0,
                          height: 90.0,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(45.0)),
                        clipBehavior: Clip.hardEdge,
                      )
                          : Icon(
                        Icons.supervised_user_circle,
                        size: 90.0,
                        color: greyColor,
                      ))
                          : Material(
                        child: Image.file(
                          avatarImageFile,
                          width: 90.0,
                          height: 90.0,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(45.0)),
                        clipBehavior: Clip.hardEdge,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.camera_alt,
                          color: primaryColor.withOpacity(0.5),
                        ),
                        onPressed: getImage,
                        padding: EdgeInsets.all(30.0),
                        splashColor: Colors.transparent,
                        highlightColor: greyColor,
                        iconSize: 30.0,
                      ),
                    ],
                  ),
                ),
                width: double.infinity,
                margin: EdgeInsets.all(20.0),
              ),

              // Input
              Column(
                children: <Widget>[
                  // Username
                  Container(
                    child: Text(
                      'Group Name',
                      style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.bold, color: primaryColor),
                    ),
                    margin: EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                  ),
                  Container(
                    child: Theme(
                      data: Theme.of(context).copyWith(primaryColor: primaryColor),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: 'Group Name',
                          contentPadding: new EdgeInsets.all(5.0),
                          hintStyle: TextStyle(color: greyColor),
                        ),
                        controller: controllerNameGroup,
                        onChanged: (value) {
                          nameGroup = value;
                        },
                        focusNode: focusNodeNameGroup,
                      ),
                    ),
                    margin: EdgeInsets.only(left: 30.0, right: 30.0),
                  ),

                  // About me
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              ),

              // Button
              Container(
                child: FlatButton(
                  onPressed: (){
                    saveData();
                  },
                  child: Text(
                    'NEXT',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  color: primaryColor,
                  highlightColor: new Color(0xff8d93a0),
                  splashColor: Colors.transparent,
                  textColor: Colors.white,
                  padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                ),
                margin: EdgeInsets.only(top: 50.0),
              ),
            ],
          ),
          padding: EdgeInsets.only(left: 15.0, right: 15.0),
        ),

        // Loading
        Positioned(
          child: isLoading
              ? Container(
            child: Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
            ),
            color: Colors.white.withOpacity(0.8),
          )
              : Container(),
        ),
      ],
    );
  }
}
