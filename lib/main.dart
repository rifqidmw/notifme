import 'package:notifme/home.dart';
import 'package:notifme/util/progress_hud.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'friend.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'const.dart';
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {

  final String title;

  const LoginPage({Key key, this.title}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  SharedPreferences prefs;

  bool isLoading = false;
  bool isLoggedIn = false;
  FirebaseUser currentUser;

  bool _isGoogleAuthEnable = false;
  bool _isEmailAuthEnable = true;
  bool _isLoading = false;

  final _teMobileEmail = TextEditingController();
  final _teCountryCode = TextEditingController();
  final _tePassword = TextEditingController();

  FocusNode _focusNodeMobileEmail = new FocusNode();
  FocusNode _focusNodeCountryCode = new FocusNode();
  FocusNode _focusNodePassword = new FocusNode();

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  @override
  void initState(){
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.getToken().then((token){
      print(token);
    });
    checkFirstOpen();
    isSignedIn();
  }

  Future checkFirstOpen() async{
    bool _first = (prefs.getBool('first') ?? false);

    if (!_first){
      prefs.setBool('first', true);

      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

      var androidPlatformChannelSpecifics =
      new AndroidNotificationDetails('repeating channel id',
          'repeating channel name', 'repeating description');
      var iOSPlatformChannelSpecifics =
      new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.periodicallyShow(0, 'repeating title',
          'repeating body', RepeatInterval.EveryMinute, platformChannelSpecifics);
    }
  }

  void addDevices(String email, String token) async{
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://172.27.13.19/firebasenoification";

    FormData formData = new FormData.from({
      "email": email,
      "token": token
    });

    response = await dio.post("/RegisterDevice.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }

  void isSignedIn() async{
    this.setState((){
      isLoading = true;
    });

    prefs = await SharedPreferences.getInstance();

    isLoggedIn = await googleSignIn.isSignedIn();
    if (isLoggedIn){
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                HomePage(currentUserId: prefs.getString('id'))),
      );
    }

    this.setState((){
      isLoading = false;
    });
  }

  Future<Null> handleSignInGoogle() async{
    _isLoading = false;
    prefs = await SharedPreferences.getInstance();

    this.setState((){
      isLoading = true;
    });

    GoogleSignInAccount googleUser =await googleSignIn.signIn()
    .catchError((e) => loginError(e));
    GoogleSignInAuthentication googleAuth =await googleUser.authentication;
    
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken
    );


    FirebaseUser firebaseUser =await firebaseAuth.signInWithCredential(credential);

    if (firebaseUser != null){
      final QuerySnapshot result = await Firestore.instance
        .collection('users')
        .where('id', isEqualTo: firebaseUser.uid)
        .getDocuments();
      final List<DocumentSnapshot> documents = result.documents;

      Fluttertoast.showToast(msg: "Sign in success");
      this.setState((){
        isLoading = false;
      });

      Future<dynamic> firebaseMessaging = FirebaseMessaging().getToken().then((token) async{
        debugPrint('token: '+token);
        addDevices(firebaseUser.email, token);

        if (documents.length == 0){
          Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .setData({
            'email' : firebaseUser.email,
            'nickname' : firebaseUser.displayName,
            'photoUrl' : firebaseUser.photoUrl,
            'id' : firebaseUser.uid,
            'token' : token
          });

          currentUser = firebaseUser;
          await prefs.setString('id', firebaseUser.uid);
          await prefs.setString('email', firebaseUser.email);
          await prefs.setString('nickname', firebaseUser.displayName);
          await prefs.setString('photoUrl', firebaseUser.photoUrl);
          await prefs.setString('token', token);
        } else {
          Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .updateData({'token': token});
          await prefs.setString('id', documents[0]['id']);
          await prefs.setString('email', documents[0]['email']);
          await prefs.setString('nickname', documents[0]['nickname']);
          await prefs.setString('photoUrl', documents[0]['photoUrl']);
          await prefs.setString('aboutMe', documents[0]['aboutMe']);
          await prefs.setString('token', token);
        }
      }).catchError((e)=> loginError(e));

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
                  currentUserId: firebaseUser.uid,
                )),
      );
    } else {
      Fluttertoast.showToast(msg: "Sign in fail");
      this.setState((){
        isLoading = false;
      });
    }
  }

  Future<Null> handleSignUpAnon() async{
    setState(() async {
      if (_isEmailAuthEnable && validateEmail(_teMobileEmail.text) == null) {
        _isLoading = true;
        FirebaseUser firebaseUser = await firebaseAuth.createUserWithEmailAndPassword(email: _teMobileEmail.text, password: _tePassword.text)
        .then((FirebaseUser user) => handleSignInAnon(_teMobileEmail.text, _tePassword.text))
        .catchError((e) => loginError(e));
        // firebaseAnonymouslyUtil
        //     .createUser(_teMobileEmail.text, _tePassword.text)
        //     .then((String user) => login(_teMobileEmail.text, _tePassword.text))
        //     .catchError((e) => loginError(e));
      }
    });
  }

  Future<Null> handleSignInAnon(String email, String pass) async {
      FirebaseUser firebaseUser = await firebaseAuth.signInWithEmailAndPassword(email: _teMobileEmail.text, password: _tePassword.text);
      var token = "";
      Future<dynamic> firebaseMessaging = FirebaseMessaging().getToken().then((token){
        token = token;
        debugPrint('token: '+token);
      }).catchError((e)=> loginError(e));
        if (firebaseUser != null){
          final QuerySnapshot result = await Firestore.instance
            .collection('users')
            .where('id', isEqualTo: firebaseUser.uid)
            .getDocuments();
          final List<DocumentSnapshot> documents = result.documents;
          if (documents.length == 0){
            Firestore.instance
              .collection('users')
              .document(firebaseUser.uid)
              .setData({
                'email' : firebaseUser.email,
                'nickname' : firebaseUser.displayName,
                'photoUrl' : firebaseUser.photoUrl,
                'id' : firebaseUser.uid,
                'token' : FirebaseMessaging().getToken()
              });
            
            currentUser = firebaseUser;
            await prefs.setString('id', currentUser.uid);
            await prefs.setString('email', currentUser.email);
            await prefs.setString('nickname', currentUser.displayName);
            await prefs.setString('photoUrl', currentUser.photoUrl);
            await prefs.setString('token', FirebaseMessaging().getToken().toString());
          } else {
            await prefs.setString('id', documents[0]['id']);
            await prefs.setString('email', documents[0]['email']);
            await prefs.setString('nickname', documents[0]['nickname']);
            await prefs.setString('photoUrl', documents[0]['photoUrl']);
            await prefs.setString('aboutMe', documents[0]['aboutMe']);
            await prefs.setString('token', FirebaseMessaging().getToken().toString());
          }
          Fluttertoast.showToast(msg: "Sign in success");
          this.setState((){
            isLoading = false;
          });
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      currentUserId: firebaseUser.uid,
                    )),
          );
        } else{
          loginError("Sign in fail");
        }
    // firebaseAnonymouslyUtil
    //     .signIn(_teMobileEmail.text, _tePassword.text)
    //     .then((FirebaseUser user) => moveUserDashboardScreen(user))
    //     .catchError((e) => loginError(e));
  }

  loginError(e) {
    setState(() {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(e),
      ));
      _isLoading = false;
    });
  }

  void gMailTabEnable() {
    setState(() {
      _isEmailAuthEnable = false;
      _isGoogleAuthEnable = true;
      _teMobileEmail.text="";
      handleSignInGoogle();
    });
  }

  void eMailTabEnable() {
    setState(() {
      _teMobileEmail.text="";
      _isEmailAuthEnable = true;
      _isGoogleAuthEnable = false;
    });
  }

  @override
  void onLoginError(String errorTxt) {
    setState(() => _isLoading = false);
  }

  @override
  void closeLoader() {
    setState(() => _isLoading = false);
  }

  @override
  void showAlert(String msg) {
    setState(() {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(msg),
      ));
    });
  }

  @override
  void showLoader() {
    setState(() => _isLoading = true);
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      showAlert("Email is Required");
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      showAlert("Invalid Email");
      return "Invalid Email";
    } else {
      return null;
    }
  }


  @override
  Widget build(BuildContext context) {
    var tabs = new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new GestureDetector(
          onTap: () {
            eMailTabEnable();
          },
          child: new Image.asset(
            "assets/image/email.png",
            height: 30.0,
            width: 30.0,
          ),
        ),
        new SizedBox(
          width: 10.0,
        ),
        new GestureDetector(
          onTap: () {
            gMailTabEnable();
          },
          child: new Image.asset(
            "assets/image/gmail.png",
            height: 30.0,
            width: 30.0,
          ),
        ),
      ],
    );

    var anonymouslyForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        new TextFormField(
          controller: _teMobileEmail,
          focusNode: _focusNodeMobileEmail,
          decoration: InputDecoration(
            labelText: "Please enter email",
            hintText: "Email",
            fillColor: new Color(0xFF2CB044),
            prefixIcon: new Icon(Icons.email),
          ),
        ),
        new SizedBox(
          height: 10.0,
        ),
        new TextFormField(
          controller: _tePassword,
          focusNode: _focusNodePassword,
          decoration: InputDecoration(
            labelText: "Password",
            hintText: "Passwrod",
            fillColor: new Color(0xFF2CB044),
            prefixIcon: new Icon(Icons.keyboard_hide),
          ),
          obscureText: true,
        ),
        new SizedBox(
          height: 10.0,
        ),
        tabs
      ],
    );

    var googleForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        new SizedBox(
          height: 20.0,
        ),
        new Center(
          child: new CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
          ),
        ),
        new SizedBox(
          height: 40.0,
        ),
        tabs
      ],
    );

    var loginForm = new Column(
      children: <Widget>[
        new Container(
          alignment: FractionalOffset.center,
          margin: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
          padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 10.0),
          decoration: new BoxDecoration(
            color: const Color.fromRGBO(255, 255, 255, 1.0),
            border: Border.all(color: const Color(0x33A6A6A6)),
            borderRadius: new BorderRadius.all(const Radius.circular(6.0)),
          ),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              _isEmailAuthEnable ? anonymouslyForm : googleForm,
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _isEmailAuthEnable
                      ? new GestureDetector(
                          onTap: () {
                            handleSignUpAnon();
                          },
                          child: new Container(
                            margin: EdgeInsets.only(top: 20.0,right: 20.0),
                            padding: EdgeInsets.all(15.0),
                            alignment: FractionalOffset.center,
                            decoration: new BoxDecoration(
                              color: new Color(0xFF2CB044),
                              borderRadius: new BorderRadius.all(
                                  const Radius.circular(6.0)),
                            ),
                            child: Text(
                              _isEmailAuthEnable ? "SIGN-UP" : "",
                              style: new TextStyle(
                                  color: const Color(0xFFFFFFFF),
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      : new SizedBox(
                          width: 0.0,
                          height: 0.0,
                        ),
                  new GestureDetector(
                    onTap: () {
                      handleSignInAnon;
                    },
                    child: new Container(
                      margin: EdgeInsets.only(top: 20.0),
                      padding: EdgeInsets.all(15.0),
                      alignment: FractionalOffset.center,
                      decoration: new BoxDecoration(
                        color: new Color(0xFF2CB044),
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(6.0)),
                      ),
                      child: Text(
                        _isEmailAuthEnable ? "LOGIN" : "SUBMIT",
                        style: new TextStyle(
                            color: const Color(0xFFFFFFFF),
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );

    var screenRoot = new Container(
      height: double.maxFinite,
      alignment: FractionalOffset.center,
      child: new SingleChildScrollView(
        child: new Center(
          child: loginForm,
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Chat App',
          style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      key: _scaffoldKey,
      body: ProgressHUD(
        child: screenRoot,
        inAsyncCall: _isLoading,
        opacity: 0.0,
      ),
    );
  }
}