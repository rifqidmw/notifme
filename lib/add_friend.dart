import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifme/chat.dart';
import 'package:notifme/friend_profile.dart';
import 'package:notifme/home.dart';
import 'package:notifme/model/friend_model.dart';
import 'package:notifme/setting.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:notifme/util/search_service.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:ui';
import 'const.dart';
import 'friend.dart';
import 'main.dart';

class AddFriend extends StatefulWidget {
  final String currentUserId;

  AddFriend({Key key, @required this.currentUserId}) : super(key: key);

  @override
  _AddFriendState createState() =>
      _AddFriendState(currentUserId: currentUserId);
}

class _AddFriendState extends State<AddFriend> {

  final String currentUserId;

  String dataSearch = '';
  bool isLoading = false;
  List<FriendModel> dataList = new List();
  List<FriendModel> backDataList = new List();

  _AddFriendState({Key key, @required this.currentUserId});

  @override
  void initState() {
    super.initState();
  }

  TextEditingController editingController = TextEditingController();
  void filterSearchResults(String query) {
    List<FriendModel> dummySearchList = List<FriendModel>();
    dummySearchList.addAll(dataList);
    if(query.isNotEmpty) {
      List<FriendModel> dummyListData = List<FriendModel>();
      dummySearchList.forEach((item) {
        if(dataList.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        dataList.clear();
        dataList.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        dataList.clear();
        dataList.addAll(backDataList);
      });
    }

  }

  Future<bool> onBackPress(){
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              FriendProfileDashboard(currentUserId: currentUserId)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Add Friend"),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              onChanged: (value) {
                filterSearchResults(value);
                dataSearch = value;
                debugPrint('Hasil search: $dataSearch');
              },
              controller: editingController,
              decoration: InputDecoration(
                  labelText: "Search",
                  hintText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
          Expanded(
            child: StreamBuilder(
              stream: Firestore.instance.collection('users').snapshots(),
              builder: (context, snapshot){
                if (!snapshot.hasData){
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                    ),
                  );
                } else{
                  return ListView.builder(
                    padding: EdgeInsets.all(10.0),
                    itemBuilder: (context, index) =>
                        buildItem(context, snapshot.data.documents[index])
                    ,
                    itemCount: snapshot.data.documents.length,
                    shrinkWrap: true,
                  );
                }
              },
            ),
          ),

          Positioned(
            child: isLoading ? Container(
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                ),
              ),
              color: Colors.white.withOpacity(0.8),
            ) : Container(),
          ),
        ],
      ),
    );
  }

  void loadData(DocumentSnapshot document){
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    friendModel.setId = document['id'];
    dataList.add(friendModel);
    backDataList.add(friendModel);
    if (friendModel.get_id == currentUserId) {
      return Container();
    } else {
        return Container(
          child: FlatButton(
            child: Row(
              children: <Widget>[
                Material(
                  child: CachedNetworkImage(
                    // placeholder: Container(
                    //   child: CircularProgressIndicator(
                    //     strokeWidth: 1.0,
                    //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                    //   ),
                    //   width: 50.0,
                    //   height: 50.0,
                    //   padding: EdgeInsets.all(15.0),
                    // ),

                    imageUrl: friendModel.get_photoUrl,
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  clipBehavior: Clip.hardEdge,
                ),
                new Flexible(
                  child: Container(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          child: Text(
                            'Nickname: ${friendModel.get_nickname}',
                            style: TextStyle(color: primaryColor),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                        ),
                        new Container(
                          child: Text(
                            '${friendModel.get_email ?? 'Not available'}',
                            style: TextStyle(color: primaryColor),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 20.0),
                  ),
                ),
              ],
            ),
            onPressed: () {
              _showDialog(friendModel.get_nickname, friendModel.get_email, friendModel.get_photoUrl, friendModel.get_id);
            },
            color: greyColor2,
            padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          ),
          margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
        );
    }
  }

  void _showDialog(String nickname, String email, String photoUrl, String id){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: new Text("Add Friend"),
          content: new Text("Are you sure want to add "+nickname+" as your friend?"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: (){
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Add"),
              onPressed: (){
                Firestore.instance
                .collection('users')
                .document(currentUserId)
                .collection('friend')
                .document(id)
                .setData({
                  'email' : email,
                  'nickname' : nickname,
                  'photoUrl' : photoUrl,
                  'id' : id
                });
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }
}
