import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class AddNotification extends StatefulWidget {
  final String idGroup;

  const AddNotification({Key key, this.idGroup}) : super(key: key);

  @override
  _AddNotificationState createState() => _AddNotificationState(idGroup: idGroup);
}

class _AddNotificationState extends State<AddNotification> {
  final String idGroup;

  _AddNotificationState({@required this.idGroup});

  TextEditingController controllerTitle = new TextEditingController();
  TextEditingController controllerMessage = new TextEditingController();


  void pushNotification() async{
    debugPrint(idGroup);
    Response response;
    var dio = Dio();
    dio.options.baseUrl = "http://172.27.13.19/firebasenoification";

    FormData formData = new FormData.from({
      "idgroup": idGroup,
      "title": controllerTitle.text,
      "message": controllerMessage.text
    });

    response = await dio.post("/sendMultiplePush.php", data: formData, options: Options(method: "POST"));
    debugPrint("response dio: "+response.data.toString());
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("ADD DATA"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new TextField(
                  controller: controllerTitle,
                  decoration: new InputDecoration(
                      hintText: "Title", labelText: "Title"),
                ),
                new TextField(
                  controller: controllerMessage,
                  decoration: new InputDecoration(
                      hintText: "Message", labelText: "Message"),
                ),
                new Padding(
                  padding: const EdgeInsets.all(10.0),
                ),
                new RaisedButton(
                  child: new Text("PUSH"),
                  color: Colors.blueAccent,
                  onPressed: () {
                    pushNotification();
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
