import 'dart:io';

import 'package:notifme/add_friend.dart';
import 'package:notifme/chat_group.dart';
import 'package:notifme/create_group_one.dart';
import 'package:notifme/model/friend_model.dart';
import 'package:notifme/model/group_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'chat.dart';
import 'setting.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'main.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'const.dart';

class GroupDashboard extends StatefulWidget {
  final String currentUserId;

  GroupDashboard({Key key, @required this.currentUserId}) : super(key: key);

  @override
  _GroupDashboardState createState() =>
      _GroupDashboardState(currentUserId: currentUserId);
}

class _GroupDashboardState extends State<GroupDashboard> {
  _GroupDashboardState({Key key, @required this.currentUserId});

  final String currentUserId;

  bool isLoading = false;

  SharedPreferences prefs;

  String userPhotoUrl = '';
  String nickname = '';

  List<GroupModel> dataList = new List();

  @override
  void initState() {
    super.initState();
    readLocal();
  }

  void readLocal() async {
    prefs = await SharedPreferences.getInstance();
    userPhotoUrl = prefs.getString('photoUrl') ?? '';
    nickname = prefs.getString('nickname') ?? '';
    // Force refresh input
  }

  Future<bool> onBackPress() {
    return Future.value(false);
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    GroupModel groupModel = new GroupModel();
    groupModel.setId = document['idGroup'];
    groupModel.setName = document['nameGroup'];
    groupModel.setPhotoUrl = document['photoUrl'];
    List<String> members = List.from(document['members']);
    dataList.add(groupModel);

    if (members.contains(currentUserId)){
      return Card(
        child: FlatButton(
          child: Row(
            children: <Widget>[
              Material(
                child: CachedNetworkImage(
                  // placeholder: Container(
                  //   child: CircularProgressIndicator(
                  //     strokeWidth: 1.0,
                  //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  //   ),
                  //   width: 50.0,
                  //   height: 50.0,
                  //   padding: EdgeInsets.all(15.0),
                  // ),

                  imageUrl: groupModel.get_photoUrl,
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                clipBehavior: Clip.hardEdge,
              ),
              new Flexible(
                child: Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        child: Text(
                          '${groupModel.get_name}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
        onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new ChatGroup(
                      groupName: document['nameGroup'],
                      peerId: document['idGroup'],
                      peerAvatar: userPhotoUrl,
                      nickName: nickname,
                      adminId: document['adminId'],
                      currentUserId: currentUserId,
                    )));
        },

          padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        color: greyColor2,
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
    } else {
      return Container();
    }
  }

  final GoogleSignIn googleSignIn = new GoogleSignIn();

  void onItemMenuPress(Choice choice) {
    if (choice.title == 'Log out') {
      handleSignOut();
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => Settings()));
    }
  }

  Future<Null> handleSignOut() async {
    this.setState(() {
      isLoading = true;
    });

    await FirebaseAuth.instance.signOut();
    await googleSignIn.disconnect();
    await googleSignIn.signOut();

    this.setState(() {
      isLoading = false;
    });

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream: Firestore.instance.collection('groups').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) {
                        return buildItem(context, snapshot.data.documents[index]);
                      },

                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading
                  ? Container(
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                        ),
                      ),
                      color: Colors.white.withOpacity(0.8),
                    )
                  : Container(),
            ),
          ],
        ),
        onWillPop: onBackPress,
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CreateGroupOne(
                        currentUserId: currentUserId,
                      )));
          dispose();
        },
        child: Icon(Icons.add),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16.0))),
      ),
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}
