import 'package:cached_network_image/cached_network_image.dart';
import 'package:notifme/model/friend_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'const.dart';

class GroupMember extends StatefulWidget {
  final String groupId;
  final String adminId;
  final String currentUserId;

  GroupMember({Key key, this.groupId, this.adminId, this.currentUserId}) : super(key: key);

  @override
  _GroupMemberState createState() => _GroupMemberState(groupId: groupId, adminId: adminId, currentUserId: currentUserId);
}

class _GroupMemberState extends State<GroupMember> {
  final String groupId;
  final String adminId;
  final String currentUserId;

  _GroupMemberState({@required this.groupId, @required this.adminId, @required this.currentUserId});

  bool isLoading = false;

  List<FriendModel> dataList = new List();

  @override
  void initState() {
    super.initState();
  }

  Future<bool> onBackPress(){
    Navigator.pop(context);
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    FriendModel friendModel = new FriendModel();
    friendModel.setNickname = document['nickname'];
    friendModel.setEmail = document['email'];
    friendModel.setPhotoUrl = document['photoUrl'];
    dataList.add(friendModel);
      return Container(
        child: FlatButton(
          child: Row(
            children: <Widget>[
              Material(
                child: CachedNetworkImage(
                  // placeholder: Container(
                  //   child: CircularProgressIndicator(
                  //     strokeWidth: 1.0,
                  //     valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  //   ),
                  //   width: 50.0,
                  //   height: 50.0,
                  //   padding: EdgeInsets.all(15.0),
                  // ),

                  imageUrl: document['photoUrl'],
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                clipBehavior: Clip.hardEdge,
              ),
              new Flexible(
                child: Container(
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        child: Text(
                          'Nickname: ${document['nickname']}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                      new Container(
                        child: Text(
                          '${document['email'] ?? 'Not available'}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
          onPressed: () {
//            Navigator.push(
//                context,
//                new MaterialPageRoute(
//                    builder: (context) => new Chat(
//                      nickname: document['nickname'],
//                      peerId: document.documentID,
//                      peerAvatar: document['photoUrl'],
//                    )));
          },
          color: greyColor2,
          padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
  }

  final GoogleSignIn googleSignIn = new GoogleSignIn();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Members"),
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            Container(
              child: StreamBuilder(
                stream: Firestore.instance.collection('groups').document(groupId).collection('members').snapshots(),
                builder: (context, snapshot){
                  if (!snapshot.hasData){
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else{
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) => buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                    );
                  }
                },
              ),
            ),
            Positioned(
              child: isLoading ? Container(
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                  ),
                ),
                color: Colors.white.withOpacity(0.8),
              ) : Container(),
            ),
          ],
        ),
        onWillPop: onBackPress,
      ),
    );
  }
}

class Choice{
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}
