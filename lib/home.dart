import 'package:notifme/add_friend.dart';
import 'package:notifme/friend.dart';
import 'package:notifme/group.dart';
import 'package:notifme/setting.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {

  final String currentUserId;

  HomePage({Key key, @required this.currentUserId}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState(currentUserId: currentUserId);
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  _HomePageState({Key key, @required this.currentUserId});

  final String currentUserId;
//  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TabController _controller;

  @override
  void initState() {
    super.initState();
//    scaffoldKey = GlobalObjectKey<ScaffoldState>(currentUserId);
    _controller = new TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
//      key: scaffoldKey,
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        iconTheme: new IconThemeData(color: const Color(0xFFFFFFFF),),
        title: new Text("Chat",
          style: new TextStyle(
            color:
            const Color(0xFFFFFFFF),
            fontSize: 20.0,
            fontWeight: FontWeight.w300,
            letterSpacing: 0.3,
          ),
        ),
        bottom: new TabBar(
          controller: _controller,
          indicatorSize: TabBarIndicatorSize.tab,
          labelColor: Color(0xFFFFFFFF),
          tabs: const <Tab>[
            const Tab(text: 'Friend'),
            const Tab(text: 'Group'),
            const Tab(text: 'Settings'),
          ],
        ),
      ),
      body: new TabBarView(
        controller: _controller,
        children: <Widget>[
          new FriendDashboard(currentUserId: currentUserId),
          new GroupDashboard(currentUserId: currentUserId,),
          new Settings(),
        ],
      ),
    );

  }
}